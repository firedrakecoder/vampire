using Vampire;

namespace VampireTest;

[TestFixture]
public class LexerTest
{
    [SetUp]
    public void Setup()
    {
        
    }

    private static List<Token> GetTokens(string input)
    {
        var lexer = new Lexer(input);
        return lexer.Tokenize();
    }

    private static void TestTokens(List<Token> expected, List<Token> actual)
    {
        Assert.That(actual, Has.Count.EqualTo(expected.Count));
        
        for(var i = 0; i < expected.Count; i++)
        {
            Assert.That(actual[i].Type, Is.EqualTo(expected[i].Type));
        }
    }
    
    [Test]
    public void TokenizeAssignment()
    {
        const string input = "var A = B;";

        List<Token> expectedTokens =
        [
            new (TokenType.Var, "var", 0,0),
            new (TokenType.Ident, "A", 0, 4),
            new (TokenType.Assign, "=", 0,6),
            new (TokenType.Ident, "B", 0,8),
            new (TokenType.SemiColon, ";", 0,9),
            new (TokenType.Eof, "", 0,10)
        ];
        
        var actualTokens = GetTokens(input);
        TestTokens(actualTokens, expectedTokens);
    }

    [Test]
    public void TokenizeConditional()
    {
        const string input = "if (A < B) {  }";
        
        List<Token> expectedTokens = [
            new (TokenType.If, "if", 0, 0),
            new (TokenType.LeftParen, "(", 0, 3),
            new (TokenType.Ident, "A", 0, 4),
            new (TokenType.LessThan, "<", 0, 6),
            new (TokenType.Ident, "B", 0, 8),
            new (TokenType.RightParen, ")", 0, 9),
            new (TokenType.LeftBrace, "{", 0, 11),
            new (TokenType.RightBrace, "}", 0, 14),
            new (TokenType.Eof, "", 0,15)
        ];
        
        var actualTokens = GetTokens(input);
        TestTokens(actualTokens, expectedTokens);
    }
    
    [Test]
    public void TokenizeIfElseConditional()
    {
        const string input = "if (A < B) {  } else {  }";
        
        List<Token> expectedTokens = [
            new (TokenType.If, "if", 0, 0),
            new (TokenType.LeftParen, "(", 0, 3),
            new (TokenType.Ident, "A", 0, 4),
            new (TokenType.LessThan, "<", 0, 6),
            new (TokenType.Ident, "B", 0, 8),
            new (TokenType.RightParen, ")", 0, 9),
            new (TokenType.LeftBrace, "{", 0, 11),
            new (TokenType.RightBrace, "}", 0, 14),
            new (TokenType.Else, "else", 0, 16),
            new (TokenType.LeftBrace, "{", 0, 21),
            new (TokenType.RightBrace, "}", 0, 24),
            new (TokenType.Eof, "", 0,25)
        ];
        
        var actualTokens = GetTokens(input);
        TestTokens(actualTokens, expectedTokens);
    }
    
    [Test]
    public void TokenizeIntegerLiteral()
    {
        const string input = "var A = 10;";
        
        List<Token> expectedTokens = [
            new (TokenType.Var, "var", 0,0),
            new (TokenType.Ident, "A", 0, 4),
            new (TokenType.Assign, "=", 0,6),
            new (TokenType.Integer, "10", 0,8),
            new (TokenType.SemiColon, ";", 0,10),
            new (TokenType.Eof, "", 0,11)
        ];
        
        var actualTokens = GetTokens(input);
        TestTokens(actualTokens, expectedTokens);
    }
    
    [Test]
    public void TokenizeFloatLiteral()
    {
        const string input = "var A = 5.23;";
        
        List<Token> expectedTokens = [
            new (TokenType.Var, "var", 0,0),
            new (TokenType.Ident, "A", 0, 4),
            new (TokenType.Assign, "=", 0,6),
            new (TokenType.Float, "5.23", 0,8),
            new (TokenType.SemiColon, ";", 0,12),
            new (TokenType.Eof, "", 0,13)
        ];
        
        var actualTokens = GetTokens(input);
        TestTokens(actualTokens, expectedTokens);
    }
    
    [Test]
    public void TokenizeArrow()
    {
        const string input = "-> - >";
        
        List<Token> expectedTokens = [
            new (TokenType.Arrow, "->", 0,0),
            new (TokenType.Subtract, "-", 0, 3),
            new (TokenType.GreaterThan, ">", 0,5),
            new (TokenType.Eof, "", 0,6)
        ];
        
        var actualTokens = GetTokens(input);
        TestTokens(actualTokens, expectedTokens);
    }
}