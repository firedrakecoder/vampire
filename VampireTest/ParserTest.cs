﻿using Vampire;
using Vampire.Nodes;

namespace VampireTest;

[TestFixture]
public class ParserTest
{
    [SetUp]
    public void Setup()
    {
        
    }
    
    [Test]
    public void ParseAssignment()
    {
        const string input = "var Test = A;";
        var lexer = new Lexer(input);
        var tokens = lexer.Tokenize();
        
        var expectedTree = new AssignmentNode(new IdentifierNode("Test"), new IdentifierNode("A"));

        var parser = new Parser(tokens);
        var actualTree = parser.Parse();
        Assert.That(actualTree, Is.EqualTo(expectedTree));
    }
}