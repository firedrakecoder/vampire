﻿namespace Vampire.Nodes;

public class OperatorNode(string @operator, ExpressionNode left, ExpressionNode right) : ExpressionNode
{
    string Operator = @operator;
    ExpressionNode Left = left;
    ExpressionNode Right = right;
}