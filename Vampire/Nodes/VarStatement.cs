﻿namespace Vampire.Nodes;

public class VarStatement(IdentifierNode identifier, ExpressionNode value) : StatementNode
{
    public IdentifierNode Identifier { get; set; } = identifier;
    public ExpressionNode Value { get; set; } = value;
}