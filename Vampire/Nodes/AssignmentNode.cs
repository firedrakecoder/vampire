﻿namespace Vampire.Nodes;

public class AssignmentNode(IdentifierNode identifier, ExpressionNode expression) : Node
{
    public IdentifierNode Identifier = identifier;
    public ExpressionNode Expression = expression;
}