﻿namespace Vampire.Nodes;

public class IdentifierNode(string identifier) : ExpressionNode
{
    public string Identifier = identifier;
}