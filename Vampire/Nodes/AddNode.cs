﻿namespace Vampire.Nodes;

public class AddNode(ExpressionNode left, ExpressionNode right) : Node
{
    public ExpressionNode Left = left, Right = right;
}