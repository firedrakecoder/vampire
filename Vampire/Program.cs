﻿using Vampire.Nodes;

namespace Vampire;

using System.CommandLine;

internal static class Program
{
    private static List<Token> ScanTokens(string input)
    {
        var lexer = new Lexer(input);
        Console.WriteLine("Scanning Tokens");
        var tokens = lexer.Tokenize();
        foreach (var token in tokens)
        {
            Console.WriteLine(token.Type + " : \"" + token.Value + "\"");
        }
        return tokens;
    }

    private static Node ParseTokens(List<Token> tokens)
    {
        Console.WriteLine("Parsing Grammar");
        var parser = new Parser(tokens);
        return parser.Parse();
    }
    
    private static async Task Main(string[] args)
    {
        var scriptOption = new Option<string>("--script", "filepath of script to execute") {IsRequired = true};
        var command = new RootCommand { scriptOption };

        command.SetHandler((scriptFile) =>
        {
            var input = File.ReadAllText(scriptFile);
            var tokens = ScanTokens(input);
            var parseTree = ParseTokens(tokens);
        }, scriptOption);
        
        await command.InvokeAsync(args);
    }
}