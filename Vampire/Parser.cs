﻿using Vampire.Nodes;

namespace Vampire;

public class Parser(List<Token> tokens)
{
    private int _position = 0;

    public Node Parse()
    {
        if (CurrentToken().Type == TokenType.Var)
        {
            return ParseVarStatement();
        }
        
        return new Node();
    }

    private void ReadToken()
    {
        _position++;
    }

    private Token CurrentToken()
    {
        return tokens[_position];
    }

    private Token PeekToken()
    {
        return tokens[_position + 1];
    }

    private VarStatement ParseVarStatement()
    {
        var identifier = ParseIdentifier();
        var value = ParseExpression();
        return new VarStatement(identifier, value);
    }

    private IdentifierNode ParseIdentifier()
    {
        var identifier = CurrentToken();
        return new IdentifierNode(identifier.Value);
    }

    private ExpressionNode ParseExpression()
    {
        //var token = _lexer.ReadToken();
        var nextToken = PeekToken();
        
        if (nextToken.Type == TokenType.Assign)
        {
            //return ParseOperatorExpression();
        }

        return new IntegerNode(0);
    }

    //private OperatorNode ParseOperatorExpression()
    //{
        //var token = _lexer.ReadToken();
        //return new OperatorNode();
    //}
}