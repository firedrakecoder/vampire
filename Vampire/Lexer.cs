﻿namespace Vampire;

public class Lexer(string input)
{
    private int _position;
    private int _lineNumber;
    private int _columnNumber;
    
    private readonly List<Token> _tokens = [];

    public List<Token> Tokenize()
    {
        while (_position < input.Length)
        {
            switch (input[_position])
            {
                case ' ':
                case '\t':
                case '\r':
                    ReadChar();
                    break;
                case '\n':
                {
                    _lineNumber++;
                    _columnNumber = 0;
                    ReadChar();
                    break;
                }
                case ',':
                    AddToken(TokenType.Comma, ",");
                    ReadChar();
                    break;
                case '.':
                    AddToken(TokenType.Period, ".");
                    ReadChar();
                    break;
                case ';':
                    AddToken(TokenType.SemiColon, ";");
                    ReadChar();
                    break;
                case '(':
                    AddToken(TokenType.LeftParen, "(");
                    ReadChar();
                    break;
                case ')':
                    AddToken(TokenType.RightParen, ")");
                    ReadChar();
                    break;
                case '[':
                    AddToken(TokenType.LeftBracket, "[");
                    ReadChar();
                    break;
                case ']':
                    AddToken(TokenType.RightBracket, "]");
                    ReadChar();
                    break;
                case '{':
                    AddToken(TokenType.LeftBrace, "{");
                    ReadChar();
                    break;
                case '}':
                    AddToken(TokenType.RightBrace, "}");
                    ReadChar();
                    break;
                case '!':
                {
                    AddToken(TokenType.Not, "!");
                    ReadChar();
                    break;
                }
                case '<':
                {
                    AddToken(TokenType.LessThan, "<");
                    ReadChar();
                    break;
                }
                case '>':
                {
                    AddToken(TokenType.GreaterThan, ">");
                    ReadChar();
                    break;
                }
                case '=':
                {
                    AddToken(TokenType.Assign, "=");
                    ReadChar();
                    break;
                }
                case '+':
                {
                    AddToken(TokenType.Add, "+");
                    ReadChar();
                    break;
                }
                case '-':
                {
                    if (PeekChar('>'))
                    {
                        AddToken(TokenType.Arrow, "->");
                        ReadChar(2);
                    }
                    else
                    {
                        AddToken(TokenType.Subtract, "-");
                        ReadChar();
                    }
                    break;
                }
                case '*':
                {
                    AddToken(TokenType.Multiply, "*");
                    ReadChar();
                    break;
                }
                case '/':
                {
                    AddToken(TokenType.Divide, "/");
                    ReadChar();
                    break;
                }
                case var _ when(char.IsLetter(input[_position])):
                {
                    ReadIdentifier();
                    break;
                }
                case var _ when(char.IsDigit(input[_position])):
                {
                    ReadNumber();
                    break;
                }
                default:
                {
                    Console.WriteLine($"Unexpected character: {input[_position]}");
                    ReadChar();
                    break;
                }
            }
        }
        
        AddToken(TokenType.Eof, "");
        
        return _tokens;
    }

    private void ReadChar(int count = 1)
    {
        _position += count;
        _columnNumber += count;
    }

    private bool PeekChar(char c)
    {
        if (_position + 1 > input.Length)
        {
            return false;
        }
        
        return input[_position + 1] == c;
    }

    private void AddToken(TokenType type, string value)
    {
        _tokens.Add(new Token(type, value, _lineNumber, _columnNumber));
    }
    
    private void ReadIdentifier()
    {
        var identifier = string.Empty;

        for (var i = _position; i < input.Length; i++)
        {
            if (char.IsLetterOrDigit(input[i]))
            {
                identifier += input[i];
            }
            else
            {
                break;
            }
        }
                
        switch (identifier)
        {
            case "var":
                AddToken(TokenType.Var, identifier);
                break;
            case "if":
                AddToken(TokenType.If, identifier);
                break;
            case "else":
                AddToken(TokenType.Else, identifier);
                break;
            default:
                AddToken(TokenType.Ident, identifier);
                break;
        }
        
        ReadChar(identifier.Length);
    }

    private TokenType GetNumberType(string number)
    {
        if (int.TryParse(number, out _))
        {
            return TokenType.Integer;
        }

        return float.TryParse(number, out _) ? TokenType.Float : TokenType.Unknown;
    }

    private void ReadNumber()
    {
        var number = string.Empty;
        
        for (var i = _position; i < input.Length; i++)
        {
            if (char.IsDigit(input[i]) || input[i] == '.')
            {
                number += input[i];
            }
            else
            {
                break;
            }
        }
        
        AddToken(GetNumberType(number), number);
        ReadChar(number.Length);
    }
}