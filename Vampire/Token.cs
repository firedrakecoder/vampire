﻿namespace Vampire;

public enum TokenType
{
    Unknown,
    Eof,
    
    //Keywords
    Var,
    If,
    Else,
    
    //Operators
    And,
    Or,
    Not,
    Equals,
    NotEquals,
    LessThan,
    GreaterThan,
    LessEquals,
    GreaterEquals,
    Add,
    Subtract,
    Multiply,
    Divide,
    
    //Punctuation
    Comma,
    Period,
    SemiColon,
    LeftParen,
    RightParen,
    LeftBracket,
    RightBracket,
    LeftBrace,
    RightBrace,
    Assign,
    Arrow,
    
    //Literals
    Ident,
    Integer,
    Float
}

public class Token(TokenType type, string value, int line, int column)
{
    public readonly TokenType Type = type;
    public readonly string Value = value;
    public readonly int Line = line;
    public readonly int Column = column;
}